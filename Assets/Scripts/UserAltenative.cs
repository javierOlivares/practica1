﻿using UnityEngine;
using System.Collections;
using SQLite4Unity3d;

public class UserAltenative{

	[PrimaryKey,AutoIncrement]
	public int id { get; set; }
	public string name { get; set; }
	public int highScore { get; set; }

	public override string ToString ()
	{
		return string.Format ("[UserAltenative: id={0}, name={1}, highScore={2}]", id, name, highScore);
	}
}
