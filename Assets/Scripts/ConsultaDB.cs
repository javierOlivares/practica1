﻿using UnityEngine;
using System.Collections;
using SQLite4Unity3d;
using System.Collections.Generic;
using System.IO;

public class ConsultaDB {
	
	private SQLiteConnection _connection = new SQLiteConnection(Application.persistentDataPath + "/TestSQLite",SQLiteOpenFlags.ReadWrite);

	public IEnumerable<UserAltenative> GetUserAlternative()
	{
		return _connection.Table<UserAltenative> ();

	}

}
